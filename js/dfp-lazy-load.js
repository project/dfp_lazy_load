/**
 * @file
 * Attaches behaviors for DFP Lazy Load module.
 */

(function ($) {
  Drupal.behaviors.dfpLazyLoad = {
    attach: function (context, settings) {
      if (context !== document) {
        return;
      }
      var config = settings.dfp_lazy_load || {};
      var refreshAd = this.refreshAd;
      // First check if there are any lazy ads configured at all
      var no_lazy_ads = Object.keys(config).every(function (ad) {
        return !config[ad];
      });
      if (no_lazy_ads) {
        // No lazy ads configured, use SRA to refresh/load all ads at once
        this.refreshAllAds(config);
      }
      else {
        Object.keys(config).forEach(function (slot) {
          if (typeof googletag.slots[slot] !== 'undefined') {
            var element = document.getElementById(googletag.slots[slot].getSlotElementId());
            if (element) {
              if (config[slot]) {
                new Waypoint.Inview({
                  element: element,
                  enter: function () {
                    refreshAd(slot, this.element);
                  }
                });
              }
              else {
                refreshAd(slot, element);
              }
            }
          }
        });
      }
    },

    refreshAd: function (slot, element) {
      if (!element.classList.contains('ad-loaded')) {
        googletag.cmd.push(function () {
          googletag.pubads().refresh([googletag.slots[slot]], {changeCorrelator: false});
        });
        element.classList.add('ad-loaded');
      }
    },

    refreshAllAds: function (config) {
      googletag.cmd.push(function () {
        googletag.pubads().refresh(null, {changeCorrelator: false});
      });
      Object.keys(config).forEach(function (slot) {
        if (typeof googletag.slots[slot] !== 'undefined') {
          var element = document.getElementById(googletag.slots[slot].getSlotElementId());
          if (element) {
            element.classList.add('ad-loaded');
          }
        }
      });
    }
  };
})(jQuery);
