<?php

/**
 * @file
 * DFP Lazy Load configuration form.
 */

/**
 * Form builder for DFP Lazy Load configuration form.
 */
function dfp_lazy_load_settings($form, $form_state) {

  $options = [];
  $tags = dfp_tag_load_all();
  foreach ($tags as $tag) {
    $options[$tag->machinename] = $tag->slot;
  }

  $form['dfp_lazy_load'] = [
    '#type' => 'checkboxes',
    '#title' => t('Enable Lazy Load for the following Ad Slots'),
    '#default_value' => variable_get('dfp_lazy_load', ''),
    '#options' => $options,
  ];

  return system_settings_form($form);
}
