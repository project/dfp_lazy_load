INTRODUCTION
------------
TBD

REQUIREMENTS
------------
This module requires the following modules to be enabled:
 * DFP (https://www.drupal.org/project/dfp)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

MAINTAINERS
-----------
Current maintainers:
 * Artem Kolotilkin - https://www.drupal.org/u/temkin
 * Artem Berdyshev - https://www.drupal.org/u/berdyshev